import {auth, db, dbFirestore} from "./firebase.js";
import {ref, set} from "https://www.gstatic.com/firebasejs/9.6.3/firebase-database.js";
import {doc, updateDoc, deleteField, getDoc} from "https://www.gstatic.com/firebasejs/9.6.3/firebase-firestore.js";


var flagLogout = false;

window.onload = () =>  {
    checkLogin();
}

function checkLogin(){
    auth.onAuthStateChanged((user) => {
        if(user){
            document.querySelector('#username').innerHTML = user.displayName;
            document.querySelector("#logout").onclick = () => {
                flagLogout = true;
                auth.signOut().then(function() {
                    flagLogout = true;
                    console.log("Signed out");
                } , function(error) {
                    console.error("Sign out Error", error);
                })
            }
            setPage(user.uid);
        }
        else{
            if(!flagLogout)
                console.log("You are not logged in, redirecting to login page");
            window.location.href = "index.html";
        }
    })

}
async function removeFromWishList(uid, data){
    var mid = data.id; 
    var title = data.title;
    var year = data.year;
    var genre = data.genre;

    var node = document.querySelector("#msg_displayed");
    node.innerHTML = "Removing from wishlist...";
    var msg = document.querySelector("#msg");
    msg.setAttribute("style","visibility:visible;");
    set(ref(db, uid + "/" + mid), {
        "genre": genre,
        "id": mid,
        "title": title,
        "year": year,
        "inWishList": false
    })
    
    const userMoviesRef = doc(dbFirestore, "wishlist", uid);
    await updateDoc(userMoviesRef, {
        [mid]: deleteField()
    });

    msg.setAttribute("style","visibility:hidden;");

    var rowId = document.querySelector('#button_table_' + mid).parentNode.parentNode.rowIndex;
    document.querySelector("#table_movies").deleteRow(rowId);
    const docRef = doc(dbFirestore, "wishlist", uid);
    const docSnap = await getDoc(docRef);
    if((Object.keys(docSnap.data()).length) == 0){
        document.querySelector("#container").setAttribute("style","visibility:hidden;");
        emptyWishList();
        document.querySelector("#msg").setAttribute("style","visibility:visible;");
    }
}

async function setPage(uid_user){

    const docRef = doc(dbFirestore, "wishlist", uid_user);
    const docSnap = await getDoc(docRef);
    var table = document.querySelector('#table_movies');
    var container = document.querySelector('#container');
    var flag;
    if(docSnap.exists()){
  
        flag = false;
        var cnt = 0;
        for(let mid in docSnap.data()){
            flag = true;
            cnt = cnt + 1;
            var movie = docSnap.data()[mid];
            var row = table.insertRow(-1);
            row.insertCell(0).innerHTML = movie.title;
            row.insertCell(1).innerHTML = movie.year;
            row.insertCell(2).innerHTML = movie.genre;
            var currId = "button_table_" + mid;
            row.insertCell(3).innerHTML = '<button id=' + currId + ' class="heart">Remove</button>';
            document.querySelector('#' + currId).onclick = () => removeFromWishList(uid_user, docSnap.data()[mid]);
        }
        if(flag){
            document.querySelector("#msg").setAttribute("style","visibility:hidden");
            container.setAttribute("style","visibility:visible;");
        }
        else{
            emptyWishList();
        }
    }
    else{
        emptyWishList();
    }           
} 


function emptyWishList(){
    var node, nodeButton;
    node = document.querySelector("#msg_displayed");
    node.innerHTML = "No movies here!";
    nodeButton = document.createElement("button");
    nodeButton.setAttribute("id","btnEmptyWishList");
    nodeButton.setAttribute("onclick","location.href='dashboard.html';");
    nodeButton.innerHTML = "Movies list";
    document.querySelector("#msg").appendChild(nodeButton);
}