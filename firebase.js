import { initializeApp 
} from "https://www.gstatic.com/firebasejs/9.6.3/firebase-app.js";
import {
  getAuth,
  GoogleAuthProvider,
  signInWithPopup,
} from "https://www.gstatic.com/firebasejs/9.6.3/firebase-auth.js";
import { getDatabase 
} from "https://www.gstatic.com/firebasejs/9.6.3/firebase-database.js";
import { getFirestore 
} from "https://www.gstatic.com/firebasejs/9.6.3/firebase-firestore.js";

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyBS8LuM7dfokgZg31p0X8lg3i0IYHDlcrg",
  authDomain: "fir-cloud5.firebaseapp.com",
  databaseURL:"https://fir-cloud5-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "fir-cloud5",
  storageBucket: "fir-cloud5.appspot.com",
  messagingSenderId: "643353791440",
  appId: "1:643353791440:web:a0e6b548b7b3a0d67287d5",
};

const app = initializeApp(firebaseConfig);


const provider = new GoogleAuthProvider();
provider.setCustomParameters({ prompt: "select_account" });

export const auth = getAuth();
export const signInWithGoogle = () => signInWithPopup(auth, provider);
export const db = getDatabase(app);
export const dbFirestore = getFirestore(app);
//firebase deploy --only hosting:cloud5-app
